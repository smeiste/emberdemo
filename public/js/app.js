var app = window.App = Ember.Application.create({
    LOG_TRANSITIONS: true
});

app.ApplicationAdapter = DS.FixtureAdapter;

/**
 * ROUTES
 */
app.Router.map(function() {
    this.resource('artists', {path: 'artists'}, function () {
        this.resource('artist', {path: ':artist_id'})
    });
    this.resource('albums', {path: 'albums'});
    this.resource('tracks', {path: 'tracks'});
});

// artists route
app.ArtistsRoute = Ember.Route.extend({
    model: function () {
        return this.store.find('artist')
    }
});

app.ArtistRoute = Ember.Route.extend({
    model: function (params) {
        return this.store.find('artist', params.artist_id)
    }
});


/**
 * MODELS
 */
var attr = DS.attr;
app.Artist = DS.Model.extend({
    name: attr('string'),
    genre: attr('string'),
    startYear: attr('number'),
    albums: DS.hasMany('album', {async: true})
});

app.Artist.FIXTURES = [{
    id: 1,
    name: 'Dream Theater',
    startYear: 1986,
    genre: 'Prog',
    albums: [1, 2, 3]
}, {
    id: 2,
    name: 'Foo Fighters',
    startYear: 1997,
    genre: 'Rock',
    albums: [4, 5]
}, {
    id: 3,
    name: 'Rush',
    genre: 'Prog',
    startYear: 1971,
    albums: [6]
}];

app.Album = DS.Model.extend({
    artistId: attr('number'),
    title: attr('string'),
    releaseYear: attr('number')
});

app.Album.FIXTURES = [{
    id: 1,
    artistId: 1,
    title: 'Images and Words',
    releaseYear: 1991
}, {
    id: 2,
    artistId: 1,
    title: 'Dream Theater',
    releaseYear: 2014
}, {
    id: 3,
    artistId: 1,
    title: 'Scenes from a Memory',
    releaseYear: 1999
}, {
    id: 4,
    artistId: 2,
    title: 'The Color and the Shape',
    releaseYear: 2001
}, {
    id: 5,
    artistId: 2,
    title: 'Wasting Light',
    releaseYear: 2011
}, {
    id: 6,
    artistId: 3,
    title: 'Moving Pictures',
    releaseYear: 1980
}];

/**
 * CONTROLLERS
 */
app.ApplicationController = Ember.ObjectController.extend({

    isArtists: function () {
        return this.get('currentPath').startsWith('artists');
    }.property('currentPath'),

    isAlbums: function () {
        return this.get('currentPath').startsWith('albums');
    }.property('currentPath'),

    isTracks: function () {
        return this.get('currentPath').startsWith('tracks');
    }.property('currentPath')
});

app.ArtistController = Ember.ObjectController.extend({
    inEditMode: false,

    actions: {
        toggleEditMode: function () {
            this.set('inEditMode', !this.get('inEditMode'));
        }
    }
});