var express = require('express'); // Express web server framework
/*var request = require('request'); // "Request" library
var querystring = require('querystring');
var cookieParser = require('cookie-parser');*/

var app = express();

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    res.sendfile('index.html', { root: __dirname + "/public" } );
});

app.listen(3002);
console.log('waiting for request...');